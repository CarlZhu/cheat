#!/bin/sh

# exit on errors
set -e

# install to user home dir
aclocal
autoconf
automake --add-missing
autoreconf

./configure --prefix=$HOME/.local

make
make install

echo " "
echo "Cheat command install to $HOME/.local/bin"
echo "You may now run:"
echo "cheat --fetch"
echo "and then"
echo "cheat --help"
